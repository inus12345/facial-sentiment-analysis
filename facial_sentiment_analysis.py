#Inus Grobler
#April 2020

import os
import cv2
import numpy as np
import label_image #This is used for prediction from retrain.py model
from mtcnn.mtcnn import MTCNN
import pandas as pd
import hashlib
#from retinaface import RetinaFace
from detect import DetectFace

IMAGES_DIR='100images_for_test' #Directory of images to process

FACE_SKIP_PERC=0.02 #The percentage of pixels a face needs to be atleast
CENTRE_PORTION=0.6 #A third centre of the image to check for centre of face

list_of_images=[] #list of images information as dictionaries
hash_no_sentiment=[] #list of the hash of images with no face detected
"""
#Uncomment to use MTCNN
#initialise the detector class.
detector = MTCNN()
"""
"""
#For RetinaFace usage with GPU
thresh = 0.8
gpuid = 0
detector = RetinaFace('model_RetinaFaceR50/R50',0,gpuid,'net3')
"""
#Using RetinaFace with CPU
detector=DetectFace(trained_model='model_RetinaFaceR50/Resnet50_Final.pth')

# Save and update csv
def update_list_of_images(photo_name,predictions,score,path_to_photo,filehash):
    """
    Updates a list of dictionaries with the information of the photos
    and the sentiment results

    Parameters
    ----------
    photo_name: string with the filename of the photo
    negative_probability: float with probability of the face being a negative sentiment
    neutral_probability: float with probability of the face being a negative sentiment
    positive_probability: float with probability of the face being a negative sentiment
    sentiment_score: float with the score of the sentiment
    path_to_photo: string with the directory path to the photo
    filehash: string with the hash of the photo
    """
    image_info={
                'photo_name':photo_name,
                'negative_probability1':predictions['negative1'],
                'neutral_probability':predictions['neutral'],
                'positive_probability1':predictions['positive1'],
                'negative_probability0.5':predictions['negative05'],
                'positive_probability0.5':predictions['positive05'],
                'sentiment_score':score,
                'path_to_photo':path_to_photo,
                'filehash':filehash
                }
    list_of_images.append(image_info)

# Save and update csv
def save_to_csv():
    """
    Updates a csv file with the sentiment results
    - if the file with the image information already exists
    and the list of images checked is greater than 0, the file is updated
    - if the list of images checked is 0, the function ends
    - if there is not a file with the image information
    a new file is created and the list of images information is added to the file

    The same is done for the hash_no_sentiment
    - This is the hashes of the photos that had no face detected
    and needs to be skipped the next time the program is run.
    """
    #Image Information
    if os.path.exists('image_info.csv') and len(list_of_images)>0:
        df_image_sentiment=pd.DataFrame(list_of_images)
        df=pd.read_csv('image_info.csv')
        df_image_sentiment=pd.concat([df_image_sentiment, df], sort=False)
        df_image_sentiment.to_csv('image_info.csv', index=False)
    elif len(list_of_images)==0:
        return #Return if there is no new images to label
    else:
        df_image_sentiment=pd.DataFrame(list_of_images)
        df_image_sentiment.to_csv('image_info.csv', index=False)

    #Hash of no sentiment photos
    if os.path.exists('hash_no_sentiment.csv') and len(hash_no_sentiment)>0:
        df_hash_no_sentiment=pd.Series(hash_no_sentiment)
        df=pd.read_csv('hash_no_sentiment.csv')
        df_hash_no_sentiment=pd.concat([df_hash_no_sentiment, df], sort=False)
        df_hash_no_sentiment.to_csv('hash_no_sentiment.csv', index=False)
    elif len(hash_no_sentiment)==0:
        return #Return if there is no new images to label
    else:
        df_hash_no_sentiment=pd.Series(hash_no_sentiment)
        df_hash_no_sentiment.to_csv('hash_no_sentiment.csv', index=False, header=False)

# Check centre image function
def check_face_center(x, y, w, h, height_img, width_img):
    """
    - This function takes as input the x and y coordinate of the bottom left corner of the face
        and the length and width of the box around the face.
    - It also takes as input the dimensions of the original image
    - It is first determined if the face is a certain percentage of the image resolution
        defined in FACE_SKIP_PERC
    - t is then determined if the center of the face in a defined center portion of the image
        defined in the constant CENTRE_PORTION
    -

    Input: x, y, w, h of face and height and width of original image
    Returns False if the face is not the main face and True if the face is the main face
    """
    image_area=height_img*width_img
    #Skip images of certain size
    if (w*h)<(FACE_SKIP_PERC*image_area):
        return False
    #Skip images not centre portion
    #4 corners of center portion each corner is a tuple of x and y coordinates
    pixles_skip=(1-CENTRE_PORTION)/2
    top_left=(width_img*pixles_skip,height_img*(1-pixles_skip))
    top_right=(width_img*(1-pixles_skip),height_img*(1-pixles_skip))
    bottom_left=(width_img*pixles_skip,height_img*pixles_skip)
    bottom_right=(width_img*(1-pixles_skip),height_img*(1-pixles_skip))
    #Get image center
    center_x = (0.5*w)+x
    center_y = (0.5*h)+y

    if center_x<top_right[0] and center_x>top_left[0]:
        if center_y<bottom_left[1] and center_y>top_left[1]:
            return False

    return True

def resize_image(img, longest):
    """
    Resize an image with the widest side of the image
    equal to longest amount of pixels

    Parameters
    ----------
    img: an image object that will be resized
    longest: an integer, the resize value of the longest size

    Returns
    --------
    Returns a resized image
    """
    height_img=img.shape[0]
    width_img=img.shape[1]

    if height_img<=longest and width_img<=longest:
        return img

    ratio=width_img/height_img

    if ratio>=1:
        scale_factor=width_img/longest
    else:
        scale_factor=height_img/longest
    #Get new dimensions of resized photo
    new_width=int(width_img/scale_factor)
    new_height=int(height_img/scale_factor)
    # dsize
    dsize = (new_width, new_height)
    # resize image
    return cv2.resize(img, dsize)

def sentiment_detection(path_to_photo, name):
    """
    Take as input a path to a photo:
    1. Read image
    2. Resize image with maximum pixels of 1000
    3. Get the hash of the photo
    4. Search for main face in the image
    5. Predict the sentiment of the face detected
    6. Output photo with a box around the face and sentiment results
    7. Update a list of dictionary with image information

    Parameters
    ----------
    path_to_photo: str
        the path to the photo to use as input

    name: str
        the name of photo
    """
    # Open photo and save to img
    img=cv2.imread(path_to_photo,1)
    #resize image with maximum pixels to 1000
    im=resize_image(img, 1000)
    del img
    #Get hash of photo
    with open(path_to_photo, 'rb') as f:
        filehash = hashlib.md5(f.read()).hexdigest()
    """
    #detect face using MTCNN
    face_locations = detector.detect_faces(im)
    """
    """
    #Using retinaface GPU
    faces,landmarks=detector.detect(im,thresh,scales=[1],do_flip=False)
    """
    #Using retinaface pytorch
    faces=detector.detect_faces(im)

    #Get height and width of input image
    height_img=im.shape[0]
    width_img=im.shape[1]

    count=0 #count of faces per photo

    # for face in zip(face_locations):
    #     x, y, w, h = face[0]['box']
        #"""
    for face in faces:
        box=face.astype(np.int)
        x=box[0]
        y=box[1]
        w=box[2]-x
        h=box[3]-y
        #"""
        #Check if the face is the main face for prediction
        #otherwise skip to next face
        face_checker=check_face_center(x, y, w, h, height_img, width_img)
        if not face_checker:
            continue
        #Draw rectangle around face
        cv2.rectangle(im, (x,y), (x+w,y+h), (50,250,50), 2)
        #Save just the rectangle face
        sub_face = im[y:y+h, x:x+w]
        FaceFileName = f"temp/{name}_{count}.png"
        cv2.imwrite(FaceFileName, sub_face)

        predictions = label_image.main(FaceFileName)# Getting the probabilitie in Dictionary
        label=''
        score=0
        font = cv2.FONT_HERSHEY_TRIPLEX
        moving=0
        for k,v in predictions.items():
            text=f"{str(k)}: {str(v)}"
            cv2.putText(im, text,(x+w+5,y+moving), font, 0.5, (0,150,0), 1)
            label=label+str(k)+str(v)
            if str(k)=='neutral':
                score+=0
            elif str(k)=='positive1':
                score+=v
            elif str(k)=='negative1':
                score-=v
            elif str(k)=='positive05':
                score+=v*0.5
            elif str(k)=='negative05':
                score-=v*0.5
            moving+=20
        average_score="Sentiment score: "+str(round(score,3))
        cv2.putText(im, average_score,(x+w+5,y+moving), font, 0.5, (250,0,250), 1)
        os.remove(FaceFileName)
        cv2.imwrite(f"output_photos/{name}_{count}_{label}.png",im)
        update_list_of_images(name,predictions,score,path_to_photo,filehash)
        count+=1

    if count==0:
        hash_no_sentiment.append(filehash)

if __name__ == '__main__':
    #Check if output_photos and temp folder exists
    #Otherwise create the folders
    if not os.path.exists('output_photos'):
        os.makedirs('output_photos')
    if not os.path.exists('temp'):
        os.makedirs('temp')

    # Open the file hashes of the photos already analysed if it exists
    if os.path.exists('image_info.csv'):
        list_of_hashes=pd.read_csv('image_info.csv')['filehash'].values.tolist()
    else:
        list_of_hashes=[]
    if os.path.exists('hash_no_sentiment.csv'):
        list_of_hashes.append(pd.read_csv('hash_no_sentiment.csv').values.tolist())
    #Go through the directory and subdirectorys of the specified folder in IMAGES_DIR
    for root, dirs, files in os.walk(IMAGES_DIR, topdown=False):
        for name in files:
            file=os.path.join(root, name)
            # Get the hash of the file
            with open(file, 'rb') as f:
                filehash = hashlib.md5(f.read()).hexdigest()
            # If the hash is not in list_of_hashes of previously opened files
            # then analyse the photo and get the sentiment results using the
            # sentiment_detection function
            if filehash not in list_of_hashes:
                list_of_hashes.append(filehash)
                try:
                    sentiment_detection(file, name[:-4])
                except Exception as e:
                    print(str(e)+': '+file)
                    continue
    save_to_csv() #Save the sentiment results to csv
